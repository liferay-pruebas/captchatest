# CaptchaTest

Prueba de API captcha de Liferay, usando el captcha propio de LF y recaptcha v2 de Google.

## Nota:

Usar
```html
<%@taglib uri="http://liferay.com/tld/captcha" prefix="liferay-captcha" %>
<liferay-captcha:captcha url="${captchaURL}" />
```

en vez de
```html
<liferay-ui:captcha url="<%=captchaURL%>" />
```