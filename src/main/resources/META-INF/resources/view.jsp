<%@ include file="/init.jsp" %>

<%@taglib uri="http://liferay.com/tld/captcha" prefix="liferay-captcha" %>

<liferay-ui:error key="captchaerror" message="text-verification-failed" />
<liferay-ui:success key="captchaok" message="Captcha correcto!" />

<portlet:actionURL  var="actionurl" name="addElement" />
<portlet:resourceURL var="captchaURL" id="/captcha"/>

<%-- <form id="<portlet:namespace/>form" action="<%= actionurl %>">
	<div class="form-group">
		<label for="<portlet:namespace/>name">Nombre:</label>
		<input class="form-control" type="text" name="<portlet:namespace/>name"/>
	</div>
	<div class="form-group">
		<label for="<portlet:namespace/>mail">Email:</label>
		<input class="form-control" type="email" name="<portlet:namespace/>mail"/>
	</div>
	<div class="form-group">
		<liferay-captcha:captcha url="<%=captchaURL%>" />
	</div>
	<input class="btn btn-default" type="submit"/>
</form> --%>

<aui:form id="form" action="<%= actionurl %>">
	<aui:input type="text" name="name" label="Nombre"/>
	<aui:input type="email" name="mail" label="Email"/>
	<liferay-captcha:captcha url="${captchaURL}" />
	<aui:button type="submit"/>
</aui:form>