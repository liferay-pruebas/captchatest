package CaptchaTest.portlet;

import CaptchaTest.constants.CaptchaTestPortletKeys;

import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author dramirez
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=CaptchaTest Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CaptchaTestPortletKeys.CaptchaTest,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CaptchaTestPortlet extends MVCPortlet {
	
	public void addElement(ActionRequest actionRequest, ActionResponse actionResponse)throws IOException, PortletException {
		try {
			System.out.println("Datos recibidos: nombre " + actionRequest.getParameter("name") + " mail " + actionRequest.getParameter("mail"));
			System.out.println("Comprobando el captcha...");
			CaptchaUtil.check(actionRequest);
			System.out.println("Captcha validado!");
			SessionMessages.add(actionRequest, "captchaok");
		} catch (CaptchaException e) {
			System.out.println("Captcha no v�lido");
			System.out.println(e.getMessage());
			//e.printStackTrace();
			SessionErrors.add(actionRequest, "captchaerror");
		}
	}
	
}