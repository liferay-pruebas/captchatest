package CaptchaTest.portlet;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;

import CaptchaTest.constants.CaptchaTestPortletKeys;

@Component(
	    property = {
	        "javax.portlet.name=" + CaptchaTestPortletKeys.CaptchaTest,
	        "mvc.command.name=/captcha"
	    },
	    service = MVCResourceCommand.class
	)
public class CaptchaMVCResourceCommand implements MVCResourceCommand{


	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws  PortletException {
		try {             
			System.out.println("Captcha pedido");
			CaptchaUtil.serveImage(resourceRequest, resourceResponse);
			return false;
		}catch (Exception e) {
			System.out.println("Captcha serve error!");
		}
		return true;
	}
}
